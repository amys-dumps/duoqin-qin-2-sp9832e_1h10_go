#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/platform/soc/soc.ap-ahb/20600000.sdio/by-name/recovery:36700160:fd4a6ee81b8c4c1fc33b0956877253f736ad762e; then
  applypatch  EMMC:/dev/block/platform/soc/soc.ap-ahb/20600000.sdio/by-name/boot:36700160:594acc3887e7f5c5a3333f8099c9e3979bfffe7e EMMC:/dev/block/platform/soc/soc.ap-ahb/20600000.sdio/by-name/recovery fd4a6ee81b8c4c1fc33b0956877253f736ad762e 36700160 594acc3887e7f5c5a3333f8099c9e3979bfffe7e:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
